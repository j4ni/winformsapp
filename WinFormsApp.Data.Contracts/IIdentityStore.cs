﻿using System.Collections.Generic;
using WinFormsApp.Models;

namespace WinFormsApp.Data.Contracts
{
    public interface IIdentityStore
    {
        IList<AppUser> AppUsers { get; }

        AppUser GetUserById(int id);
        void UpdateUser(AppUser appUser);
    }
}