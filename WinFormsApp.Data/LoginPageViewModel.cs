﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;

namespace WinFormsApp.Data
{
    [POCOViewModel]
    public class LoginPageViewModel
    {
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }

        protected LoginPageViewModel() { }

        public static LoginPageViewModel Create()
        {
            return ViewModelSource.Create(() => new LoginPageViewModel());
        }
    }
}