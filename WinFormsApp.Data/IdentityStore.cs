﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WinFormsApp.Data.Contracts;
using WinFormsApp.Models;

namespace WinFormsApp.Data
{
    public class IdentityStore : IIdentityStore
    {
        private readonly string _fileName;
        private readonly string _fullPath;
        public IList<AppUser> AppUsers { get; } = new List<AppUser>();

        public IdentityStore(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName) || fileName.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
                throw new Exception($"{nameof(IdentityStore)}: Invalid filename specified");

            _fileName = fileName;
            _fullPath = Path.Combine(Directory.GetCurrentDirectory(), fileName);

            if (File.Exists(_fullPath))
            {
                AppUsers = JsonConvert.DeserializeObject<List<AppUser>>(File.ReadAllText(_fullPath));
            }
            else
            {
                AppUsers = GetSampleData();
                Persist();
            }
        }

        public AppUser GetUserById(int id) => AppUsers.SingleOrDefault(user => user.Id == id);

        public void UpdateUser(AppUser appUser)
        {
            if (appUser?.Id > 0)
            {
                var stored = GetUserById(appUser.Id);

                if (stored != null)
                {
                    var indexToUpdate = AppUsers.IndexOf(stored);
                    AppUsers[indexToUpdate] = appUser;

                    Persist();
                    return;
                }
            }

            throw new Exception($"{nameof(IdentityStore)}: error when updating id: {appUser?.Id}");
        }

        private IList<AppUser> GetSampleData() =>
            new List<AppUser>
            {
                new AppUser { Id=1, UserName = "admin", Password="pwd",  FirstName= "Pista", Surname = "Kovács", City= "Budapest", BirthDate = new DateTime(1990,1,1)},
                new AppUser { Id=2, UserName = "admin2", Password="pwd",  FirstName= "Jóska", Surname = "Kovács", City= "Budapest", BirthDate = new DateTime(1990,2,2)}
            };



        private void Persist()
        {
            File.WriteAllText(_fullPath, JsonConvert.SerializeObject(AppUsers));
        }
    }
}
