﻿using System;
using System.Windows.Forms;
using WinFormsApp.Data.Contracts;
using WinFormsApp.Data;

namespace WinFormsApp2
{
    static class Program
    {
        public static IIdentityStore IdentityStore { get; private set; } 



        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            IdentityStore = new IdentityStore("userdata2.json");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginPage());
        }
    }
}
