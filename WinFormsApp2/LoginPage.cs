﻿using System;
using System.Linq;
using System.Windows.Forms;
using WinFormsApp.Data.Contracts;

namespace WinFormsApp2
{
    public partial class LoginPage : DevExpress.XtraEditors.XtraForm
    {
        private IIdentityStore _identityStore;

        public LoginPage()
        {
            _identityStore = Program.IdentityStore;
            InitializeComponent();
        }

        private void OnLoginClicked(object sender, EventArgs e)
        {
            var nameInput = _usernameBox.Text;
            var passwordInput = _passwordBox.Text;

            _errorLabel.Text = string.Empty;

            if (ValidateLogin(nameInput, passwordInput) > 0)
            {
                var listPage = new ListPage(nameInput, this);
                this.Hide();
                listPage.Show();
            }
            else
            {
                _errorLabel.Text = "could not find user";
            }
        }       
        

        private int ValidateLogin(string username, string password)
        {
            var appUser = _identityStore.AppUsers.FirstOrDefault(user => user.UserName == username && user.Password == password);

            return appUser?.Id ?? 0;
        }

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
