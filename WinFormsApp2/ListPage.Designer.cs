﻿namespace WinFormsApp2
{
    partial class ListPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._userName = new System.Windows.Forms.Label();
            this._dataGridView1 = new System.Windows.Forms.DataGridView();
            this._logoutBtn = new System.Windows.Forms.Button();
            this._fileExportBtn = new System.Windows.Forms.Button();
            this._editUserButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // _userName
            // 
            this._userName.AutoSize = true;
            this._userName.Location = new System.Drawing.Point(509, 11);
            this._userName.MinimumSize = new System.Drawing.Size(200, 0);
            this._userName.Name = "_userName";
            this._userName.Size = new System.Drawing.Size(200, 17);
            this._userName.TabIndex = 0;
            this._userName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _dataGridView1
            // 
            this._dataGridView1.AllowUserToAddRows = false;
            this._dataGridView1.AllowUserToDeleteRows = false;
            this._dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dataGridView1.Location = new System.Drawing.Point(16, 56);
            this._dataGridView1.MultiSelect = false;
            this._dataGridView1.Name = "_dataGridView1";
            this._dataGridView1.ReadOnly = true;
            this._dataGridView1.RowHeadersWidth = 51;
            this._dataGridView1.RowTemplate.Height = 24;
            this._dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._dataGridView1.Size = new System.Drawing.Size(693, 221);
            this._dataGridView1.TabIndex = 2;
            // 
            // _logoutBtn
            // 
            this._logoutBtn.Location = new System.Drawing.Point(16, 283);
            this._logoutBtn.Name = "_logoutBtn";
            this._logoutBtn.Size = new System.Drawing.Size(75, 23);
            this._logoutBtn.TabIndex = 3;
            this._logoutBtn.Text = "Logout";
            this._logoutBtn.UseVisualStyleBackColor = true;
            this._logoutBtn.Click += new System.EventHandler(this.OnLogoutClicked);
            // 
            // _fileExportBtn
            // 
            this._fileExportBtn.Location = new System.Drawing.Point(634, 283);
            this._fileExportBtn.Name = "_fileExportBtn";
            this._fileExportBtn.Size = new System.Drawing.Size(75, 23);
            this._fileExportBtn.TabIndex = 4;
            this._fileExportBtn.Text = "Export";
            this._fileExportBtn.UseVisualStyleBackColor = true;
            this._fileExportBtn.Click += new System.EventHandler(this.OnExportButtonClicked);
            // 
            // _editUserButton
            // 
            this._editUserButton.Location = new System.Drawing.Point(553, 282);
            this._editUserButton.Name = "_editUserButton";
            this._editUserButton.Size = new System.Drawing.Size(75, 23);
            this._editUserButton.TabIndex = 5;
            this._editUserButton.Text = "Edit";
            this._editUserButton.UseVisualStyleBackColor = true;
            this._editUserButton.Click += new System.EventHandler(this.OnEditUserButtonClicked);
            // 
            // List
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 335);
            this.Controls.Add(this._editUserButton);
            this.Controls.Add(this._fileExportBtn);
            this.Controls.Add(this._logoutBtn);
            this.Controls.Add(this._dataGridView1);
            this.Controls.Add(this._userName);
            this.Name = "List";
            this.Text = "List";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _userName;
        private System.Windows.Forms.DataGridView _dataGridView1;
        private System.Windows.Forms.Button _logoutBtn;
        private System.Windows.Forms.Button _fileExportBtn;
        private System.Windows.Forms.Button _editUserButton;
    }
}