﻿namespace WinFormsApp2
{
    partial class EditUserPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._bornDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this._usernameBox = new System.Windows.Forms.MaskedTextBox();
            this._passwordBox = new System.Windows.Forms.MaskedTextBox();
            this._surnameBox = new System.Windows.Forms.MaskedTextBox();
            this._firstnameBox = new System.Windows.Forms.MaskedTextBox();
            this._cityComboBox = new System.Windows.Forms.ComboBox();
            this._idLabel = new System.Windows.Forms.Label();
            this._updateButton = new System.Windows.Forms.Button();
            this._cancelButton = new System.Windows.Forms.Button();
            this._errorMsgLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _bornDateTimePicker
            // 
            this._bornDateTimePicker.Location = new System.Drawing.Point(31, 182);
            this._bornDateTimePicker.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this._bornDateTimePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this._bornDateTimePicker.Name = "_bornDateTimePicker";
            this._bornDateTimePicker.Size = new System.Drawing.Size(200, 22);
            this._bornDateTimePicker.TabIndex = 0;
            this._bornDateTimePicker.Value = new System.DateTime(1980, 1, 1, 18, 12, 0, 0);
            // 
            // _usernameBox
            // 
            this._usernameBox.Location = new System.Drawing.Point(31, 67);
            this._usernameBox.Name = "_usernameBox";
            this._usernameBox.ReadOnly = true;
            this._usernameBox.Size = new System.Drawing.Size(200, 22);
            this._usernameBox.TabIndex = 1;
            // 
            // _passwordBox
            // 
            this._passwordBox.BeepOnError = true;
            this._passwordBox.Location = new System.Drawing.Point(31, 96);
            this._passwordBox.Name = "_passwordBox";
            this._passwordBox.PasswordChar = '*';
            this._passwordBox.Size = new System.Drawing.Size(200, 22);
            this._passwordBox.TabIndex = 2;
            this._passwordBox.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // _surnameBox
            // 
            this._surnameBox.Location = new System.Drawing.Point(31, 125);
            this._surnameBox.Name = "_surnameBox";
            this._surnameBox.Size = new System.Drawing.Size(200, 22);
            this._surnameBox.TabIndex = 3;
            // 
            // _firstnameBox
            // 
            this._firstnameBox.Location = new System.Drawing.Point(31, 154);
            this._firstnameBox.Name = "_firstnameBox";
            this._firstnameBox.Size = new System.Drawing.Size(200, 22);
            this._firstnameBox.TabIndex = 4;
            // 
            // _cityComboBox
            // 
            this._cityComboBox.FormattingEnabled = true;
            this._cityComboBox.Location = new System.Drawing.Point(31, 211);
            this._cityComboBox.Name = "_cityComboBox";
            this._cityComboBox.Size = new System.Drawing.Size(200, 24);
            this._cityComboBox.TabIndex = 5;
            // 
            // _idLabel
            // 
            this._idLabel.AutoSize = true;
            this._idLabel.Location = new System.Drawing.Point(28, 47);
            this._idLabel.Name = "_idLabel";
            this._idLabel.Size = new System.Drawing.Size(0, 17);
            this._idLabel.TabIndex = 6;
            // 
            // _updateButton
            // 
            this._updateButton.Location = new System.Drawing.Point(31, 242);
            this._updateButton.Name = "_updateButton";
            this._updateButton.Size = new System.Drawing.Size(75, 23);
            this._updateButton.TabIndex = 8;
            this._updateButton.Text = "Update";
            this._updateButton.UseVisualStyleBackColor = true;
            this._updateButton.Click += new System.EventHandler(this.OnUpdateButtonClicked);
            // 
            // _cancelButton
            // 
            this._cancelButton.Location = new System.Drawing.Point(113, 242);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(75, 23);
            this._cancelButton.TabIndex = 9;
            this._cancelButton.Text = "Cancel";
            this._cancelButton.UseVisualStyleBackColor = true;
            this._cancelButton.Click += new System.EventHandler(this.OnCancelButtonClicked);
            // 
            // _errorMsgLabel
            // 
            this._errorMsgLabel.AutoSize = true;
            this._errorMsgLabel.ForeColor = System.Drawing.Color.Red;
            this._errorMsgLabel.Location = new System.Drawing.Point(31, 272);
            this._errorMsgLabel.Name = "_errorMsgLabel";
            this._errorMsgLabel.Size = new System.Drawing.Size(0, 17);
            this._errorMsgLabel.TabIndex = 10;
            // 
            // EditUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this._errorMsgLabel);
            this.Controls.Add(this._cancelButton);
            this.Controls.Add(this._updateButton);
            this.Controls.Add(this._idLabel);
            this.Controls.Add(this._cityComboBox);
            this.Controls.Add(this._firstnameBox);
            this.Controls.Add(this._surnameBox);
            this.Controls.Add(this._passwordBox);
            this.Controls.Add(this._usernameBox);
            this.Controls.Add(this._bornDateTimePicker);
            this.Name = "EditUserForm";
            this.Text = "EditUserForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnFormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker _bornDateTimePicker;
        private System.Windows.Forms.MaskedTextBox _usernameBox;
        private System.Windows.Forms.MaskedTextBox _passwordBox;
        private System.Windows.Forms.MaskedTextBox _surnameBox;
        private System.Windows.Forms.MaskedTextBox _firstnameBox;
        private System.Windows.Forms.ComboBox _cityComboBox;
        private System.Windows.Forms.Label _idLabel;
        private System.Windows.Forms.Button _updateButton;
        private System.Windows.Forms.Button _cancelButton;
        private System.Windows.Forms.Label _errorMsgLabel;
    }
}