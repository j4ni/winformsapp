﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WinFormsApp.Data.Contracts;
using WinFormsApp.Models;

namespace WinFormsApp2
{
    public partial class EditUserPage : Form
    {
        private readonly AppUser _editModel;
        private readonly Form _parent;
        private IIdentityStore _identityStore;

        public EditUserPage(AppUser editModel, Form parent)
        {
            _identityStore = Program.IdentityStore;
            _editModel = editModel;
            _parent = parent;

            InitializeComponent();

            PopulateForm();            
        }

        private void OnUpdateButtonClicked(object sender, EventArgs e)
        {
            var pwd = _passwordBox.Text;
            var surname = _surnameBox.Text;
            var firstname = _firstnameBox.Text;
            var born = _bornDateTimePicker.Value;
            var city = (string)_cityComboBox.SelectedItem;

            var validationResult = ValidateForm(pwd, surname, firstname, born);

            if (string.IsNullOrEmpty(validationResult))
            {
                _errorMsgLabel.Text = string.Empty;

                UpdateUiModel(pwd, surname, firstname, born, city);
                _identityStore.UpdateUser(_editModel);

                this.Hide();
                _parent.Show();
            }
            else
            {
                _errorMsgLabel.Text = validationResult;
            }
        }

        private void UpdateUiModel(string password, string surname, string firstname, DateTime birthDay, string city)
        {
            _editModel.Password = password;
            _editModel.Surname = surname;
            _editModel.FirstName = firstname;
            _editModel.BirthDate = birthDay;
            _editModel.City = city;
        }


        private void OnCancelButtonClicked(object sender, EventArgs e)
        {
            this.Hide();
            _parent.Show();
        }

        private void OnFormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }


        private string ValidateForm(string password, string surname, string firstname, DateTime birthDay)
        {
            var errorlist = new List<string>();

            var pwdValidation = ValidatePassword(password);
            if (!string.IsNullOrEmpty(pwdValidation))
                errorlist.Add(pwdValidation);


            var name0Validation = ValidateName(surname);
            if (!string.IsNullOrEmpty(name0Validation))
                errorlist.Add(name0Validation);

            var name1Validation = ValidateName(firstname);
            if (!string.IsNullOrEmpty(name1Validation))
                errorlist.Add(name1Validation);

            var birthDayValidation = ValidateBirthday(birthDay);
            if (!string.IsNullOrEmpty(birthDayValidation))
                errorlist.Add(birthDayValidation);

            return string.Join(", ", errorlist);
        }

        private string ValidatePassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                return "Password must not be empty";

            else if (password.Length < 3)
                return "Password must be at least 3 chars long";

            else 
                return null;
        }

        private string ValidateName(string name)
        {
            if (string.IsNullOrEmpty(name))
                return null;
            
            else if (!char.IsUpper(name[0]))
                return "name should begin with uppercase letter";

            else if (name.ToLower().Any(c => !char.IsLetter(c)))
                return "only letters allowed in name";

            else 
                return null;
        }

        private string ValidateBirthday(DateTime birthDate)
        {
            var currentDateTime = DateTime.Now;

            if (currentDateTime - birthDate < TimeSpan.FromDays(18 * 365.24))
                return "too young";

            else if (currentDateTime - birthDate > TimeSpan.FromDays(120 * 365.24))
                return "too old";

            else return null;
        }

        private void PopulateForm()
        {
            _idLabel.Text = "Id:\t"+_editModel.Id.ToString();
            _usernameBox.Text = _editModel.UserName;
            _passwordBox.Text = _editModel.Password;
            _surnameBox.Text = _editModel.Surname;
            _firstnameBox.Text = _editModel.FirstName;
            _bornDateTimePicker.Value = _editModel.BirthDate;
            _cityComboBox.Items.AddRange(LoadCitySelectionFromDB());

            _cityComboBox.SelectedItem = _editModel.City;
        }

        private string[] LoadCitySelectionFromDB() => new string[] { "Bécs", "Budapest", "Pécs" };

    }
}
