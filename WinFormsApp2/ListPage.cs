﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using WinFormsApp.Models;

namespace WinFormsApp2
{
    public partial class ListPage : Form
    {
        private readonly Form _parent;         

        public IList<AppUser> AppUsers { get; private set; }
        public ListPage(string userName, Form parent)
        {
            AppUsers = Program.IdentityStore.AppUsers;
            InitializeComponent();

            _userName.Text = userName;

            _dataGridView1.DataSource = AppUsers;
            _parent = parent;
        }

        private void OnLogoutClicked(object sender, EventArgs e)
        {
            this.Hide();
            _parent.Show();            
        }

        private void OnExportButtonClicked(object sender, EventArgs e)
        {
            using (var saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "XML document|*.xml";
                saveFileDialog.Title = "Save to XML";
                saveFileDialog.ShowDialog();

                if (!string.IsNullOrWhiteSpace(saveFileDialog.FileName))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(List<AppUser>));

                    using (var fs = File.OpenWrite(saveFileDialog.FileName))
                    {                        
                           xs.Serialize(fs, AppUsers);
                    }
                }
            }
        }

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void OnEditUserButtonClicked(object sender, EventArgs e)
        {
            if (_dataGridView1.SelectedRows.Count == 1)
            {
                int rowIndex = _dataGridView1.CurrentRow.Index;
                var user = AppUsers[rowIndex];


                var editForm = new EditUserPage(user, this);
                this.Hide();
                editForm.Show();
            }

        }
    }
}
