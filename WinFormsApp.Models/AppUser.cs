﻿using System;

namespace WinFormsApp.Models
{
    public class AppUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public DateTime BirthDate { get; set; }
        public string City { get; set; }
    }
}
